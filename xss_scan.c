#include <stdio.h>
#include <string.h>
#include <curl/curl.h>
#include <glib.h>

GString* scriptResult = NULL;

// Определение strcasestr, если не поддерживается на текущей системе
char* strcasestr(const char* haystack, const char* needle) {
    size_t needle_len = strlen(needle);
    while (*haystack) {
        if (strncasecmp(haystack, needle, needle_len) == 0) {
            return (char*)haystack;
        }
        haystack++;
    }
    return NULL;
}

// Функция для URL-кодирования значения параметра запроса
char* urlEncode(const char* str) {
    CURL* curl = curl_easy_init();
    if (curl) {
        char* output = curl_easy_escape(curl, str, 0);
        curl_easy_cleanup(curl);
        return output;
    }
    return NULL;
}

// Обработчик для данных, полученных в результате запроса
size_t WriteCallback(void* contents, size_t size, size_t nmemb, GString* buffer) {
    size_t realsize = size * nmemb;
    g_string_append_len(buffer, contents, realsize);
    return realsize;
}

// Проверка уязвимостей на XSS
void checkXSS(const char* data, const char* type) {
    // Более тщательная проверка на XSS
    const char* scriptTag = strstr(data, "<script>");
    const char* onEvent = strcasestr(data, "on");
    
    if (scriptTag != NULL || onEvent != NULL) {
        printf("\033[1;31mPotential XSS found in %s!\033[0m\n", type);

        // Выводим строки с уязвимостью
        if (scriptTag != NULL) {
            printf("\033[1;33mFound in the vicinity of '<script>':\n%s\033[0m\n", scriptTag);
        }
        if (onEvent != NULL) {
            printf("\033[1;33mFound in the vicinity of 'on' event attribute:\n%s\033[0m\n", onEvent);
        }
    }
}

// Проверка результатов выполнения скрипта и вывод сообщения об уязвимости
void checkScriptResult(const char* type) {
    if (scriptResult != NULL) {
        // Проверяем наличие уязвимости в полученных данных
        checkXSS(scriptResult->str, type);
        // Выводим только фрагмент с уязвимостью
        printf("\n%s Test Result:\n%s\n", type, scriptResult->str);

        g_string_free(scriptResult, TRUE);
        scriptResult = NULL;
    }
}

// Тестирование отражаемых XSS уязвимостей
void testReflectedXSS(const char* url) {
    CURL* curl;
    CURLcode res;
    GString* buffer = g_string_new("");

    curl = curl_easy_init();
    if (curl) {
        // Задаем URL для запроса
        curl_easy_setopt(curl, CURLOPT_URL, url);

        // Устанавливаем параметры для работы с HTTP
        curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
        curl_easy_setopt(curl, CURLOPT_USERAGENT, "Mozilla/5.0");
        curl_easy_setopt(curl, CURLOPT_COOKIEFILE, ""); // Пустая строка для использования куки-сессии

        // Задаем обработчик данных
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteCallback);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, buffer);

        // Выполняем запрос
        res = curl_easy_perform(curl);

        // Проверяем результат выполнения запроса
        if (res != CURLE_OK) {
            fprintf(stderr, "curl_easy_perform() failed: %s\n", curl_easy_strerror(res));
        } else {
            // Проверяем HTTP-код ответа
            long http_code = 0;
            curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &http_code);
            if (http_code == 200) {
                // Анализируем полученные данные на отражаемые XSS
                checkXSS(buffer->str, "Reflected XSS");
            } else {
                fprintf(stderr, "Failed to retrieve data. HTTP Code: %ld\n", http_code);
            }
        }

        // Проверяем результат выполнения скрипта
        checkScriptResult("Reflected XSS");

        // Освобождаем ресурсы
        curl_easy_cleanup(curl);
        g_string_free(buffer, TRUE);
    }
}

// Тестирование хранимых XSS уязвимостей
void testStoredXSS(const char* url, const char* scriptBody) {
    CURL* curl;
    CURLcode res;
    GString* buffer = g_string_new("");

    curl = curl_easy_init();
    if (curl) {
        // Задаем URL для запроса
        curl_easy_setopt(curl, CURLOPT_URL, url);

        // Устанавливаем параметры для работы с HTTP
        curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
        curl_easy_setopt(curl, CURLOPT_USERAGENT, "Mozilla/5.0");
        curl_easy_setopt(curl, CURLOPT_COOKIEFILE, ""); // Пустая строка для использования куки-сессии

        // Задаем обработчик данных
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteCallback);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, buffer);

        // Задаем метод POST и передаем тело запроса (скрипт)
        curl_easy_setopt(curl, CURLOPT_POST, 1);

        // Формируем тело POST-запроса с вставленным скриптом
        char postData[512];
        snprintf(postData, sizeof(postData), "input=%s", urlEncode(scriptBody));

        curl_easy_setopt(curl, CURLOPT_POSTFIELDS, postData);

        // Выполняем запрос
        res = curl_easy_perform(curl);

        // Проверяем результат выполнения запроса
        if (res != CURLE_OK) {
            fprintf(stderr, "curl_easy_perform() failed: %s\n", curl_easy_strerror(res));
        } else {
            // Проверяем HTTP-код ответа
            long http_code = 0;
            curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &http_code);
            if (http_code == 200) {
                // Анализируем полученные данные на хранимые XSS
                checkXSS(buffer->str, "Stored XSS");
            } else {
                fprintf(stderr, "Failed to retrieve data. HTTP Code: %ld\n", http_code);
            }
        }

        // Проверяем результат выполнения скрипта
        checkScriptResult("Stored XSS");

        // Освобождаем ресурсы
        curl_easy_cleanup(curl);
        g_string_free(buffer, TRUE);
    }
}

int main(void) {
    // Введите URL вашего тестируемого веб-приложения
    char targetUrl[512] = "";

    printf("Insert full URL (e.g., 'http://127.0.0.1:42001/vulnerabilities/xss_r/'): ");
    scanf("%511[^\n]%*c", targetUrl);

    printf("Injected URL: %s\n", targetUrl);

    // Ensure the URL is in the correct format with a scheme (http or https)
    if (strncmp(targetUrl, "http://", 7) != 0 && strncmp(targetUrl, "https://", 8) != 0) {
        fprintf(stderr, "Invalid URL format. Please include the scheme (http/https).\n");
        return 1;
    }

    // Тестирование отражаемых XSS уязвимостей
    printf("\033[1;34mTesting Reflected XSS...\033[0m\n");
    testReflectedXSS(targetUrl);

    // Тестирование хранимых XSS уязвимостей
    printf("\n\033[1;34mTesting Stored XSS...\033[0m\n");

    // Введите тело скрипта
    char scriptBody[512] = "alert('XSS');";  // Вставляем скрипт alert
    // char scriptBody[512];
    // printf("Enter the script body: ");
    // scanf("%511[^\n]%*c", scriptBody);

    testStoredXSS(targetUrl, scriptBody);

    return 0;
}
